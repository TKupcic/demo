package com.healthtracker.demo.controllers;

import com.healthtracker.demo.models.User;
import com.healthtracker.demo.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class UserRestController {

    @Autowired
    UserRepository repository;

    @GetMapping("/getUser")
    public Optional<User> getUser(@RequestParam(value="id") long id) {
        return repository.findById(id);
    }
}
