package com.healthtracker.demo.controllers;

import com.healthtracker.demo.models.User;
import com.healthtracker.demo.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Optional;

@RestController
public class UserWebController {

    @Autowired
    UserRepository repository;

    @RequestMapping("/save")
    public String process(){

        repository.saveAll(Arrays.asList(
                new User("Adam", "Johnson"),
                new User("Kim", "Smith"),
                new User("David", "Williams"),
                new User("Peter", "Davis")));

        return "Done";
    }


    @RequestMapping("/findall")
    public String findAll(){
        StringBuilder result = new StringBuilder();

        for(User cust : repository.findAll()){
            result.append(cust.toString()).append("<br>");
        }

        return result.toString();
    }

    @RequestMapping("/findbylastname")
    public String fetchDataByLastName(@RequestParam("lastname") String lastName){
        StringBuilder result = new StringBuilder();

        for(User cust: repository.findByLastName(lastName)){
            result.append(cust.toString()).append("<br>");
        }

        return result.toString();
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<User> users(@PathVariable(value="id") long id) {
        Optional<User> user = repository.findById(id);
        return user
                .map(user1 -> new ResponseEntity<>(user1, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/users")
    public ResponseEntity<Iterable<User>> users() {
        Iterable<User> users = repository.findAll();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PostMapping("/users")
    User addUser(@RequestBody @Valid User user) {
        return repository.save(user);
    }
}